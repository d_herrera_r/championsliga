package com.diego.championsliga;


/**
 * Created by Diego on 09-09-2016.
 */
public class Jugador {

    private String rut;
    private String nombre;
    private String apodo;
    private int nro_camiseta;
    private String posicion;
    private String fecha_nac;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(String fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public int getNro_camiseta() {
        return nro_camiseta;
    }

    public void setNro_camiseta(int nro_camiseta) {
        this.nro_camiseta = nro_camiseta;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }


    public Jugador(String rut, String nombre, String apodo, int nro_camiseta, String posicion, String fecha_nac) {
        this.rut = rut;
        this.nombre = nombre;
        this.apodo = apodo;
        this.nro_camiseta = nro_camiseta;
        this.posicion = posicion;
        this.fecha_nac = fecha_nac;
    }
}
