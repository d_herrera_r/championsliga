package com.diego.championsliga;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregarEquipoActivity extends AppCompatActivity {

    private EditText txtNombreEquipo, txtFechaCreacion, txtUrlImagen, txtEntrenador, txtColorCamiseta;
    private Button btnCrear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_equipo);

        txtNombreEquipo=(EditText)findViewById(R.id.txtNombreEquipo);
        txtFechaCreacion=(EditText)findViewById(R.id.txtFechaCreacion);
        txtUrlImagen=(EditText)findViewById(R.id.txtUrlImagen);
        txtEntrenador=(EditText)findViewById(R.id.txtEntrenador);
        txtColorCamiseta=(EditText)findViewById(R.id.txtColorCamiseta);

        btnCrear=(Button)findViewById(R.id.btnCrear);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearEquipo();
            }
        });
    }

    private void crearEquipo() {
        boolean esCorrecto=true;
        if(txtColorCamiseta.getText().toString().length()<1){
            txtColorCamiseta.setError("Debe ingresar el color");
            esCorrecto=false;
        }
        if(txtEntrenador.getText().toString().length()<1){
            txtEntrenador.setError("Debe ingresar el entrenador");
            esCorrecto=false;
        }
        if(txtFechaCreacion.getText().toString().length()<1){
            txtFechaCreacion.setError("Debe ingresar la fecha de creación");
            esCorrecto=false;
        }else if(!txtFechaCreacion.getText().toString().matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
            txtFechaCreacion.setError("Formato de fecha incorrecto. Ej 12/12/1990");
            esCorrecto=false;
        }
        if(txtNombreEquipo.getText().toString().length()<1){
            txtNombreEquipo.setError("Debe ingresar el nombre");
            esCorrecto=false;
        }
        if(txtUrlImagen.getText().toString().length()<1){
            txtUrlImagen.setError("Debe ingresar la url imagen");
            esCorrecto=false;
        }else if(!URLUtil.isValidUrl(txtUrlImagen.getText().toString())){
            txtUrlImagen.setError("Formato url incorrecto. Ej http://pagina.com");
            esCorrecto=false;
        }

        if(esCorrecto) {
            Equipo nuevoEquipo = new Equipo("1",
                    txtNombreEquipo.getText().toString(),
                    txtFechaCreacion.getText().toString(),
                    txtUrlImagen.getText().toString(),
                    txtColorCamiseta.getText().toString(),
                    txtEntrenador.getText().toString());
            if(BD.getInstance().agregarEquipo(nuevoEquipo)){
                Toast.makeText(AgregarEquipoActivity.this, "Equipo agregado", Toast.LENGTH_SHORT).show();
                limpiarCampos();
            }else{
                Toast.makeText(AgregarEquipoActivity.this, "Equipo ya existe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void limpiarCampos() {
        txtFechaCreacion.setText("");
        txtUrlImagen.setText("");
        txtNombreEquipo.setText("");
        txtColorCamiseta.setText("");
        txtEntrenador.setText("");
    }
}
