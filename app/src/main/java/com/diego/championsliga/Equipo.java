package com.diego.championsliga;

import java.util.ArrayList;

/**
 * Created by Diego on 09-09-2016.
 */
public class Equipo {

    private String uid;
    private String nombre;
    private String fecha_creacion;
    private String url_ins;
    private String color_camiseta;
    private String entrenador;
    private ArrayList<Jugador> jugadores;

    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(ArrayList<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUrl_ins() {
        return url_ins;
    }

    public void setUrl_ins(String url_ins) {
        this.url_ins = url_ins;
    }

    public String getColor_camiseta() {
        return color_camiseta;
    }

    public void setColor_camiseta(String color_camiseta) {
        this.color_camiseta = color_camiseta;
    }

    public String getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(String entrenador) {
        this.entrenador = entrenador;
    }

    public Equipo(String uid, String nombre, String fecha_creacion, String url_ins, String color_camiseta, String entrenador) {
        this.uid = uid;
        this.nombre = nombre;
        this.fecha_creacion = fecha_creacion;
        this.url_ins = url_ins;
        this.color_camiseta = color_camiseta;
        this.entrenador = entrenador;
        this.jugadores = new ArrayList<>();
    }

    public void agregarJugador(Jugador nuevo_jugador){
        this.jugadores.add(nuevo_jugador);
    }
}
