package com.diego.championsliga;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnInsJugador, btnCrearEquipo, btnListaEquipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnInsJugador=(Button)findViewById(R.id.btnInscribirJugador);
        btnCrearEquipo=(Button)findViewById(R.id.btnCrearEquipo);
        btnListaEquipos=(Button)findViewById(R.id.btnListaEquipos);
        btnInsJugador.setOnClickListener(this);
        btnCrearEquipo.setOnClickListener(this);
        btnListaEquipos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i=null;
        switch(v.getId()){
            case R.id.btnCrearEquipo:
                i=new Intent(this,AgregarEquipoActivity.class);
                break;
            case R.id.btnInscribirJugador:
                i=new Intent(this,InscribirJugadorActivity.class);
                break;
            case R.id.btnListaEquipos:
                i=new Intent(this,ListaActivity.class);
                break;
        }
        startActivity(i);
    }
}
