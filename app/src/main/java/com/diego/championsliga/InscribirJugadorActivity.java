package com.diego.championsliga;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class InscribirJugadorActivity extends AppCompatActivity {

    private Spinner cboEquipo, cboPosicion;
    private EditText txtNombreJugador, txtRut, txtFechaNac, txtApodo, txtNumeroCamiseta;
    private Button btnInscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscribir_jugador);

        cboEquipo = (Spinner)findViewById(R.id.cboEquipo);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, BD.getInstance().getNombresEquipos()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboEquipo.setAdapter(spinnerArrayAdapter);

        cboPosicion=(Spinner)findViewById(R.id.cboPosicion);

        txtNombreJugador=(EditText)findViewById(R.id.txtNombreJugador);
        txtRut=(EditText)findViewById(R.id.txtRut);
        txtFechaNac=(EditText)findViewById(R.id.txtFechaNacimiento);
        txtApodo=(EditText)findViewById(R.id.txtApodo);
        txtNumeroCamiseta=(EditText)findViewById(R.id.txtNumeroCamiseta);

        btnInscribir=(Button) findViewById(R.id.btnInscribir);

        btnInscribir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inscribirJugador();
            }
        });
    }

    private void inscribirJugador() {

        boolean esCorrecto=true;
        if(txtNombreJugador.getText().toString().length()<1){
            txtNombreJugador.setError("Debe ingresar el nombre");
            esCorrecto=false;
        }
        if(txtRut.getText().toString().length()<1){
            txtRut.setError("Debe ingresar el rut");
            esCorrecto=false;
        }
        if(txtFechaNac.getText().toString().length()<1){
            txtFechaNac.setError("Debe ingresar la fecha nacimiento");
            esCorrecto=false;
        }else if(!txtFechaNac.getText().toString().matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
            txtFechaNac.setError("Formato de fecha incorrecto. Ej 12/12/1990");
            esCorrecto=false;
        }
        if(txtApodo.getText().toString().length()<1){
            txtApodo.setError("Debe ingresar el apodo");
            esCorrecto=false;
        }
        if(txtNumeroCamiseta.getText().toString().length()<1){
            txtNumeroCamiseta.setError("Debe ingresar el número de camiseta");
            esCorrecto=false;
        }else if(txtNumeroCamiseta.getText().toString().length()>2){
            txtNumeroCamiseta.setError("EL número de camiseta debe ser de 2 dígitos");
            esCorrecto=false;
        }
        if(esCorrecto){
            Jugador jugador= new Jugador(
                    txtRut.getText().toString(),
                    txtNombreJugador.getText().toString(),
                    txtApodo.getText().toString(),
                    Integer.parseInt(txtNumeroCamiseta.getText().toString()),
                    cboPosicion.getSelectedItem().toString(),
                    txtFechaNac.getText().toString());
            BD.getInstance().agregarJugador(cboEquipo.getSelectedItem().toString(),jugador);
            Toast.makeText(InscribirJugadorActivity.this, "Jugador agregado", Toast.LENGTH_SHORT).show();
            limpiarCampos();
        }
    }

    private void limpiarCampos() {
        txtNombreJugador.setText("");
        txtRut.setText("");
        txtFechaNac.setText("");
        txtApodo.setText("");
        txtNumeroCamiseta.setText("");
    }
}
