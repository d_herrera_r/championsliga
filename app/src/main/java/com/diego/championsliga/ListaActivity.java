package com.diego.championsliga;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ListaActivity extends AppCompatActivity {

    private ListView lvEquipos;
    private EquiposAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        lvEquipos=(ListView)findViewById(R.id.lvListaEquipos);
        adapter=new EquiposAdapter(this,BD.getInstance().listaEquipos());
        lvEquipos.setAdapter(adapter);

    }
}
