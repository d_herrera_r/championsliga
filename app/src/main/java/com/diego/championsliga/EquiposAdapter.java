package com.diego.championsliga;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Diego on 15-09-2016.
 */

public class EquiposAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Equipo> dataSource;

    public EquiposAdapter(Context context, ArrayList<Equipo> dataSource) {
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.equipos_item, parent, false);
        }

        // Set data into the view.
        TextView txtNombreEquipo=(TextView) rowView.findViewById(R.id.tvNombre);
        TextView txtAnoEquipo=(TextView) rowView.findViewById(R.id.tvAno);
        TextView txtJugadores=(TextView) rowView.findViewById(R.id.tvJugadores);
        RelativeLayout rlBase=(RelativeLayout)rowView.findViewById(R.id.rlBase);

        Equipo item = this.dataSource.get(position);
        txtNombreEquipo.setText(item.getNombre());
        txtAnoEquipo.setText(item.getFecha_creacion());

        String jug="Jugadores: \n";
        for(Jugador jugador:item.getJugadores()){
            jug+=jugador.getNombre()+" - N° camiseta: "+jugador.getNro_camiseta()+"\n";
        }

        txtJugadores.setText(jug);
        /*
        int color=Color.WHITE;
        if(item.getColor_camiseta().equalsIgnoreCase("rojo")){
            color=Color.RED;
        }else if(item.getColor_camiseta().equalsIgnoreCase("azul")){
            color=Color.BLUE;
        }else if(item.getColor_camiseta().equalsIgnoreCase("verde")){
            color=Color.GREEN;
        }else if(item.getColor_camiseta().equalsIgnoreCase("amarillo")){
            color=Color.YELLOW;
        }

        rlBase.setBackgroundColor(color);
        */
        return rowView;


    }
}
