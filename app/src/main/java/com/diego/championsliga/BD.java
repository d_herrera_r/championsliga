package com.diego.championsliga;

import java.util.ArrayList;

/**
 * Created by Diego on 09-09-2016.
 */
public class BD {

    private static BD instance;
    private static ArrayList<Equipo> equipos = new ArrayList<>();

    private BD(){}

    public static BD getInstance(){
        if(instance==null){
            instance=new BD();
        }
        return instance;
    }

    public boolean agregarEquipo(Equipo equipo){
        Equipo aux=buscarEquipoPorNombre(equipo.getNombre());
        if(aux==null){
            equipos.add(equipo);
            return true;
        }else{
            return false;
        }

    }

    public void agregarJugador(String nombre_equipo,Jugador jugador){
        Equipo equipo=buscarEquipoPorNombre(nombre_equipo);
        if(equipo!=null){
            equipo.agregarJugador(jugador);
        }

    }
    public Equipo buscarEquipoPorNombre(String nombre_equipo){
        for(int i=0;i<equipos.size();i++){
            if(equipos.get(i).getNombre().equals(nombre_equipo)){
                return equipos.get(i);
            }
        }
        return null;
    }

    public ArrayList<String> getNombresEquipos(){
        ArrayList<String> nombres = new ArrayList<>();
        for (Equipo equipo:equipos) {
            nombres.add(equipo.getNombre());
        }
        return nombres;
    }

    public ArrayList<Equipo> listaEquipos(){
        return equipos;
    }
}
